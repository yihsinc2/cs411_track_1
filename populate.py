import os
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'mysite.settings')

import django
django.setup()

from pokemon.models import Base_Stat, Strength, Weakness, Location

def populate_Base_Stat(data_path):
    f = open(data_path, 'r')
    for line in f:
        line = line.replace('\n', '')
        parsed_line = line.split()
        # pdb.set_trace()
        p = Base_Stat(pokemon_name=parsed_line[0],
                      pokedex_number=parsed_line[1],
                      base_hp = parsed_line[2],
                      base_attack = parsed_line[3],
                      base_defense = parsed_line[4],
                      base_special_attack = parsed_line[5],
                      base_special_defense = parsed_line[6],
                      base_speed = parsed_line[7],
                      pokemon_pic=parsed_line[8],
                      type_1 = parsed_line[9],
                      type_2 = parsed_line[10] if len(parsed_line)==11 else 'None'
                      )
        p.save()

effective_dict = {'normal': [],
                  'fighting': ['normal', 'rock', 'steel', 'ice'],
                  'flying': ['fighting', 'bug', 'grass'],
                  'poison': ['grass', 'fairy'],
                  'ground': ['poison', 'rock', 'steel', 'fire', 'electric'],
                  'rock': ['flying', 'bug', 'fire', 'ice'],
                  'bug': ['grass', 'psychic'],
                  'ghost': ['ghost', 'psychic'],
                  'fire': ['bug', 'steel', 'ice'],
                  'water': ['ground', 'rock', 'fire'],
                  'grass': ['ground', 'rock', 'water'],
                  'electric': ['flying', 'water'],
                  'psychic': ['fighting', 'poison'],
                  'ice': ['flying', 'ground', 'grass', 'dragon'],
                  'dragon': ['dragon'],
                  'fairy': ['fighting', 'dragon'],
                  'steel': ['rock', 'fairy', 'ice']
                 }

def populate_Strength_and_Weakness():

    for type, strength in effective_dict.items():
        weakness = []
        for k, v in effective_dict.items():
            if type in v:
                weakness += [k]

        if len(strength) == 0:
            p = Strength(type=type, strength="none")
            p.save()
        else:
            for s in strength:
                p = Strength(type=type, strength=s)
                p.save()

        if len(weakness) == 0:
            p = Weakness(type=type, weakness="none")
            p.save()
        else:
            for w in weakness:
                p = Weakness(type=type, weakness=w)
                p.save()

def populate_location_info():

    with open('others/locations.txt', 'r') as f:
        for line in f:
            parsedLine = line.split('_')
            p = Location(pokedex_number = int(parsedLine[0]),
                         route_id = int(parsedLine[1]),
                         rarity = parsedLine[2])
            p.save()

if __name__ == '__main__':
    populate_location_info()