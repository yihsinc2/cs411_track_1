# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.contrib.auth.models import User

# Create your models here.
class UserProfile(models.Model):
    # the build-in User class contains username, pw, email, first name,
    # and surname as its attributes
    user = models.OneToOneField(User)

    GENDER_CHOICES = [('M', 'Male'), ('F', 'Female')]
    join_date = models.DateField(auto_now_add=True)
    gender = models.CharField(max_length=1, choices=GENDER_CHOICES)
    region = models.CharField(max_length=50, default='United States')
    picture = models.ImageField(upload_to='profile_images', blank=True)

    def __unicode__(self):
        return self.user.email


class Rank(models.Model):
    TITLE_CHOICES = [('Beginner','Beginner'),
                     ('Intermediate', 'Intermediate'),
                     ('Advanced', 'Advanced'),
                     ('Master', 'Master')]
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    title = models.CharField(max_length = 12, choices=TITLE_CHOICES)
    wins = models.IntegerField(default=0)
    loses = models.IntegerField(default=0)

class Strength(models.Model):
    type = models.CharField(max_length=20)
    strength = models.CharField(max_length=20)

class Weakness(models.Model):
    type = models.CharField(max_length=20)
    weakness = models.CharField(max_length=20)

class Try(models.Model):
    type = models.CharField(max_length=20)

class Base_Stat(models.Model):
    pokedex_number = models.IntegerField(primary_key=True)
    pokemon_name = models.CharField(max_length=50)
    base_attack = models.IntegerField()
    base_defense = models.IntegerField()
    base_hp = models.IntegerField()
    base_speed = models.IntegerField()
    base_special_attack = models.IntegerField()
    base_special_defense = models.IntegerField()
    type_1 = models.CharField(max_length=20)
    type_2 = models.CharField(max_length=20)
    location = models.CharField(max_length=50, default='Unknown')
    pokemon_pic = models.CharField(max_length = 200, default = 'Unknown')

class Location(models.Model):
    pokedex_number = models.IntegerField()
    route_id = models.IntegerField()
    rarity = models.CharField(max_length=20)

#class PC_Box(models.Model):
#    pc_box_id = models.IntegerField(default=1)
#    user = models.ForeignKey(User, on_delete=models.CASCADE)
#
#    class Meta:
#        unique_together = (("pc_box_id", "user"),)

class Pokemon(models.Model):
    base_stat = models.ForeignKey(Base_Stat, on_delete=models.CASCADE)
    type_1 = models.CharField(max_length=20)
    type_2 = models.CharField(max_length=20)
    attack = models.IntegerField()
    defense = models.IntegerField()
    hp = models.IntegerField()
    speed = models.IntegerField()
    level = models.IntegerField()
    special_attack = models.IntegerField()
    special_defense = models.IntegerField()
    #pc_box = models.ForeignKey(PC_Box, on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    # pokedex_number = models.IntegerField()

class Team(models.Model):
    team_id = models.IntegerField(default=1)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    pokemon = models.ManyToManyField(Pokemon, db_table="Team_Has")
    class Meta:
        unique_together = (("team_id", "user"),)