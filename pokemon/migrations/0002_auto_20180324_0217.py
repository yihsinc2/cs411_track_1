# -*- coding: utf-8 -*-
# Generated by Django 1.11.11 on 2018-03-24 02:17
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('pokemon', '0001_initial'),
    ]

    operations = [
        migrations.AlterUniqueTogether(
            name='pc_box',
            unique_together=set([]),
        ),
        migrations.RemoveField(
            model_name='pc_box',
            name='user',
        ),
        migrations.RemoveField(
            model_name='pokemon',
            name='pc_box',
        ),
        migrations.DeleteModel(
            name='PC_Box',
        ),
    ]
