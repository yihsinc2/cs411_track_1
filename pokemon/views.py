# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect, HttpResponse
from django.db.models import Q
from django.core.exceptions import ValidationError
from django.db.models import Q
from django.db.models import OuterRef, Subquery

import numpy as np

from forms import UserForm, UserProfileForm, PokemonSearchForm
from models import *
import pdb

# Create your views here.
def index(request):
    return render(request, 'pokemon/index.html',{})

def register(request):

    registered = False

    if request.method == 'POST':
        user_form = UserForm(data=request.POST)
        profile_form = UserProfileForm(data=request.POST)

        if user_form.is_valid() and profile_form.is_valid():
            user = user_form.save()
            user.set_password(user.password)
            user.save()

            profile = profile_form.save(commit=False)
            profile.user = user

            if 'picture' in request.FILES:
                profile.picture = request.FILES['picture']

            profile.save()
            registered = True

        else:
            print(user_form.errors, profile_form.errors)
    else:
        user_form = UserForm()
        profile_form = UserProfileForm()

    return render(request,
                  'pokemon/register.html',
                  {'user_form': user_form, "profile_form": profile_form, 'registered':registered})

def user_login(request):
    if request.method == 'POST':
        username = request.POST.get('username')
        password = request.POST.get('password')

        user = authenticate(username=username, password=password)

        if user:
            if user.is_active:
                login(request, user)
                #return (request, 'pokemon/login.html', {})
                return HttpResponseRedirect('/pokemon/')
            else:
                return HttpResponse("You account is expired!")
        else:
            return HttpResponse("Invalid login details supplied!")
    else:
        return render(request, 'pokemon/login.html', {})

@login_required
def user_logout(request):
    logout(request)
    return HttpResponseRedirect('/pokemon/')

def search_location(request):

    if request.method == "POST":
        query_dict = request.POST
        pokemon = []

        # search by pokedex name
        if query_dict.get("action") == 'By Name':
            pokemon_name = query_dict.get('pokemon_name').strip()
            try:

                queried_pokemon = Base_Stat.objects.get(pokemon_name__exact=pokemon_name)
            except Exception as e:
                return render(request, 'pokemon/search.html', {"locate_err": "invalid pokemon name", "search_form": PokemonSearchForm()})

            pokedex_number = queried_pokemon.pokedex_number
            pokemon_pic = queried_pokemon.pokemon_pic
            query = Location.objects.filter(pokedex_number__exact=pokedex_number)
            output_list = []
            if not query.exists():
                return render(request, 'pokemon/search.html',
                              {"locate_err": "can not obtain pokemon walking around", "search_form": PokemonSearchForm()})
            for q in query:
                output_list.append("%s___%s___%d___%s" % (pokemon_name,
                                                          pokemon_pic,
                                                          q.route_id,
                                                          q.rarity.replace('\n', '')))

                pokemon.append(Location_poke(str(pokemon_name), "/../../static/" + str(pokemon_pic), "Route " + str(q.route_id),
                                        str(q.rarity.replace('\n',''))))

        # search by type
        elif query_dict.get("action") == 'By Type':

            type = query_dict.get('type').lower().strip()
            queried_pokemons = Base_Stat.objects.filter(Q(type_1__exact=type) | Q(type_2__exact=type))\
                .values('pokedex_number', 'pokemon_name', 'pokemon_pic')
            pokedex_number_list = []
            pokemon_name_list = []
            pokemon_pic_list = []

            for q in queried_pokemons:
                pokedex_number_list.append(q.get("pokedex_number"))
                pokemon_name_list.append(q.get("pokemon_name"))
                pokemon_pic_list.append(q.get("pokemon_pic"))

            queried_locations = Location.objects.filter(pokedex_number__in=pokedex_number_list)

            output_list = []





            for q in queried_locations:
                index = pokedex_number_list.index(q.pokedex_number)
                output_list.append("%s___%s___%d___%s" % (pokemon_name_list[index],
                                                          pokemon_pic_list[index],
                                                          q.route_id,
                                                          q.rarity.replace('\n','')))

                pokemon.append(Location_poke(str(pokemon_name_list[index]), "/../../static/" + str(pokemon_pic_list[index]), "Route " + str(q.route_id),
                                        str(q.rarity.replace('\n',''))))


        # search by route_id
        else:
            route_id = query_dict.get('route_id')
            query = Location.objects.filter(route_id__exact=route_id).order_by('pokedex_number')
            pokedex_number_list = []
            for q in query:
                pokedex_number_list.append(q.pokedex_number)
            matched_pokemons = \
                Base_Stat.objects.filter(pokedex_number__in=pokedex_number_list).order_by('pokedex_number')

            output_list = []



            for i, p in enumerate(matched_pokemons):
                output_list.append("%s___%s___%d___%s" % (p.pokemon_name,
                                                          p.pokemon_pic,
                                                          query[i].route_id,
                                                          query[i].rarity.replace('\n', '')))
                pokemon.append(Location_poke(str(p.pokemon_name), "/../../static/" + str(p.pokemon_pic), "Route " + str(query[i].route_id),
                                        str(query[i].rarity.replace('\n',''))))
            print(output_list)

        output_pokemon_list = [pokemon[0]]
        b = 0
        e = 0
        ind = 0

        for i in range(len(pokemon)):
            if (pokemon[b].name != pokemon[i].name):
                e = i
                output_pokemon_list.append(pokemon[e])
                for j in range(b+1,e):
                    output_pokemon_list[ind].route += "\n" + pokemon[j].route
                    output_pokemon_list[ind].rarity += "\n" + pokemon[j].rarity
                ind+=1
                b = e
            elif i + 1 == len(pokemon):
                for j in range(b+1,len(pokemon)):
                    output_pokemon_list[ind].route += "\n" + pokemon[j].route
                    output_pokemon_list[ind].rarity += "\n" + pokemon[j].rarity

        return render(request, 'pokemon/location.html', {"queried_locations": output_list, "pokemon_list": output_pokemon_list})


def search_pokemon(request):
    if request.method == "POST":
        search_form = PokemonSearchForm(request.POST)
        if search_form.is_valid():
            pokedex = search_form.cleaned_data.get("pokedex")
            pokename = search_form.cleaned_data.get("pokename")
            type_1 = search_form.cleaned_data.get("type_1")
            type_2 = search_form.cleaned_data.get("type_2")
            print(type_1)
            query = Base_Stat.objects.all()
            if pokedex is None and len(pokename) == 0 and type_2 == "none" and type_1 == "none":
                return render(request, 'pokemon/search.html',
                              {"find_err": "can not search by nothing", "search_form": search_form})

            if pokedex is not None:
                if (0 <= pokedex <= 151):
                    query = query.filter(pokedex_number__exact=pokedex)
                else:
                    return render(request, 'pokemon/search.html',
                                  {"find_err": "wrong pokedex number", "search_form": search_form})




            if len(pokename) > 0:
                query = query.filter(pokemon_name__exact=pokename)

                if not query.exists():
                    # raise ValidationError("pokemon name invalid",)
                    search_form = PokemonSearchForm()
                    return render(request, 'pokemon/search.html', {"find_err": "wrong pokemon name", "search_form": search_form})

            if type_1 != "none" and type_2 == "none":
                query = query.filter(Q(type_1__exact=type_1) | Q(type_2__exact=type_1))
            if type_2 != "none" and type_1 == "none":
                query = query.filter(Q(type_1__exact=type_2) | Q(type_2__exact=type_2))
            if type_1 != "none" and type_2 != "none":
                query = query.filter((Q(type_1__exact=type_2) & Q(type_2__exact=type_1)) |
                                     (Q(type_2__exact=type_2) & Q(type_1__exact=type_1)))


            if not query.exists():
                return render(request, 'pokemon/search.html',
                              {"find_err": "I am assumming that type of pokemon does not exist check type 1 and 2", "search_form": search_form})

            return render(request, 'pokemon/search_result.html', {"queried_pokemons":query})


            #return HttpResponseRedirect('/pokemon/')
            #return render(request, '/pokemon/search_result.html', {})
        else:
            print(search_form.errors)
    else:
        search_form = PokemonSearchForm()
        return render(request, 'pokemon/search.html', {"search_form": search_form, "find_err": ""})

def search_by_stat(request):
    query_dict = request.POST
    # stat = PokemonSearchForm(request.POST).cleaned_data.get("stat")
    stat = request.POST.get("action")
    if stat == "total_stat":
        stat = "base_attack + base_defense + base_special_attack + base_special_defense + base_hp + base_speed"
    else:
        stat = "base_" + stat
    type = query_dict.get('type').lower().strip()




    return search_poke_stat(request, str(stat), type=type)

def search_poke_stat(request, stat, type):
    query = Base_Stat.objects.all()
    query = query.order_by(stat).reverse()

    if type != "all":
        query = query.filter(Q(type_1__exact=type) | Q(type_2__exact=type))


    return render(request, 'pokemon/stat.html', {"query": query})

@login_required
def addTeams(request):
    query = Team.objects.filter(user__username__exact=request.user.username).order_by('team_id')
    teamNum = len(query)
    team_id = 1
    if teamNum == 6:
        return HttpResponse("You cannot have more than 6 teams")
    elif teamNum == 0:
        team_id = 1
    else:
        j = 0
        for i in range(6):
            if i+1 != query[min(j, len(query)-1)].team_id:
                team_id = i+1
                break
            else:
                j += 1
    t = Team(team_id = team_id)
    t.user = request.user
    t.save()
    return HttpResponseRedirect('/pokemon/team/')

@login_required
def deleteTeams(request):
    team_id = request.POST.get('team_id')
    query = Team.objects.filter(user__username__exact=request.user.username,
                                team_id__exact=team_id)
    for q in query:
        q.delete()
    return HttpResponseRedirect('/pokemon/team/')

@login_required
def addPokemon(request):
    def calculate_hp(base_hp, level):
        return int(round(base_hp * level / 50. + level + 10))
    def calculate_stats(base, level):
        return int(round(base * level / 50. + 5))

    team_id = request.POST.get('team_id')
    pokemon_name = request.POST.get('pokemon_name')
    try:

        level = int(request.POST.get('level'))
    except Exception as e:
        level = 50

    if level < 0:
        level = 0
    if level > 100:
        level = 100

    team = Team.objects.get(user__username__exact=request.user.username,
                            team_id__exact=team_id)



    query = Team.objects.filter(user__username__exact=request.user.username).order_by('team_id')


    # get the base stats of the selected pokemon

    try:
        base_pokemon = Base_Stat.objects.get(pokemon_name__exact=pokemon_name)

    except Exception as e:
        render(request, 'pokemon/team.html', {"err": "invalid pokemon name"},)
        return HttpResponseRedirect('/pokemon/team/')

    base_hp = int(base_pokemon.base_hp)
    base_attack = int(base_pokemon.base_attack)
    base_defense = int(base_pokemon.base_defense)
    base_speed = int(base_pokemon.base_speed)
    base_special_attack = int(base_pokemon.base_special_attack)
    base_special_defense = int(base_pokemon.base_special_defense)
    type_1 = str(base_pokemon.type_1)
    type_2 = str(base_pokemon.type_2)

    # calculate the stats according to pokemon's level
    hp = calculate_hp(base_hp, level)
    attack = calculate_stats(base_attack, level)
    defense = calculate_stats(base_defense, level)
    speed = calculate_stats(base_speed, level)
    special_attack = calculate_stats(base_special_attack, level)
    special_defense = calculate_stats(base_special_defense, level)

    added_pokemon = Pokemon(base_stat = base_pokemon,
                            type_1=type_1,
                            type_2=type_2,
                            user = request.user,
                            level = level,
                            hp = hp,
                            attack = attack,
                            defense = defense,
                            speed = speed,
                            special_attack = special_attack,
                            special_defense = special_defense)
    added_pokemon.save()
    #added_pokemon.team_set.add(team)
    team.pokemon.add(added_pokemon)

    return HttpResponseRedirect('/pokemon/team/')

@login_required
def updatePokemon(request):
    def calculate_hp(base_hp, level):
        return int(round(base_hp * level / 50. + level + 10))
    def calculate_stats(base, level):
        return int(round(base * level / 50. + 5))

    id = request.POST.get('id')
    pokemon = Pokemon.objects.get(id__exact=id)

    if request.POST.get('action') == 'Update Level':
        level = int(request.POST.get('level'))
        base_hp = int(pokemon.base_stat.base_hp)
        base_attack = int(pokemon.base_stat.base_attack)
        base_defense = int(pokemon.base_stat.base_defense)
        base_speed = int(pokemon.base_stat.base_speed)
        base_special_attack = int(pokemon.base_stat.base_special_attack)
        base_special_defense = int(pokemon.base_stat.base_special_defense)

        # calculate the stats according to pokemon's level
        hp = calculate_hp(base_hp, level)
        attack = calculate_stats(base_attack, level)
        defense = calculate_stats(base_defense, level)
        speed = calculate_stats(base_speed, level)
        special_attack = calculate_stats(base_special_attack, level)
        special_defense = calculate_stats(base_special_defense, level)

        pokemon.level = level
        pokemon.hp = hp
        pokemon.attack = attack
        pokemon.defense = defense
        pokemon.speed = speed
        pokemon.special_attack = special_attack
        pokemon.special_defense = special_defense
        pokemon.save()
    elif request.POST.get('action') == 'Remove':
        team_id = request.POST.get('team_id')
        team = Team.objects.get(user__username__exact=request.user.username,
                                team_id__exact=team_id)
        team.pokemon.remove(pokemon)
    elif request.POST.get('action') == 'Add':
        team_id = request.POST.get('team_id')
        team = Team.objects.get(user__username__exact=request.user.username,
                                team_id__exact=team_id)
        team.pokemon.add(pokemon)

    return HttpResponseRedirect('/pokemon/team/')



@login_required
def showTeams(request):
    if request.user.is_authenticated():
        query = Team.objects.filter(user__username__exact=request.user.username).order_by('team_id')
        team_info = []
        for q in query:
            pokemons_on_the_team = Pokemon.objects.filter(team__id__exact=q.id)
            for p in pokemons_on_the_team:
                print(p.base_stat.pokemon_name)
            team_info += [{q.team_id: pokemons_on_the_team}]
        your_pokemons = Pokemon.objects.filter(user__username__exact=request.user.username)\
            .order_by('level')
        return render(request, 'pokemon/team.html', {"team_info": team_info, "your_pokemons": your_pokemons})

@login_required
def pickBestNext(request):
    team_id = request.POST.get('team_id')
    team_id = str(team_id)

    try:
        level = int(request.POST.get('level'))
    except Exception as e:
        level = 50

    if level < 0:
        level = 0

    if level>100:
        level = 100

    team = Team.objects.filter(user__username__exact=request.user.username,
                            team_id__exact=team_id)

    query = Team.objects.filter(user__username__exact=request.user.username).order_by('team_id')
    teamNum = len(query)

    t_id = -1
    for q in query:
        if int(team_id) == q.team_id:
            t_id = q.id

    if teamNum == 6:
        return HttpResponse("You cannot have more than 6 teams")

    query = 'SELECT * ' \
            'FROM pokemon_base_stat pb ' \
            'WHERE ((pb.type_1 NOT IN ( SELECT pb.type_1' \
            '                          FROM Team_Has t, pokemon_base_stat pb, pokemon_pokemon p' \
            '                          WHERE t.team_id = %s AND p.id = t.pokemon_id AND pb.pokedex_number = p.base_stat_id)) AND ' \
                    '(pb.type_1 NOT IN (SELECT pb.type_2' \
            '                          FROM Team_Has t, pokemon_base_stat pb, pokemon_pokemon p' \
            '                          WHERE t.team_id = %s AND p.id = t.pokemon_id AND pb.pokedex_number = p.base_stat_id)) AND ' \
                    '(pb.type_2 NOT IN ( SELECT pb.type_1' \
            '                          FROM Team_Has t, pokemon_base_stat pb, pokemon_pokemon p' \
            '                          WHERE t.team_id = %s AND p.id = t.pokemon_id AND pb.pokedex_number = p.base_stat_id)) AND ' \
                    '(pb.type_2 NOT IN ( SELECT pb.type_2' \
            '                          FROM Team_Has t, pokemon_base_stat pb, pokemon_pokemon p' \
            '                          WHERE t.team_id = %s AND p.id = t.pokemon_id AND pb.pokedex_number = p.base_stat_id AND pb.type_2 != "None"))) ' \
            'ORDER BY (pb.base_attack + pb.base_defense + pb.base_hp + pb.base_speed + pb.base_special_attack + pb.base_special_defense) DESC'

    top_poke = Base_Stat.objects.raw(query, [t_id, t_id, t_id, t_id])
    top = top_poke.model
    t = top_poke.columns
    poke = list()
    for p in top_poke:
        print(p.pokemon_name)
        poke.append(p)

    def calculate_hp(base_hp, level):
        return int(round(base_hp * level / 50. + level + 10))
    def calculate_stats(base, level):
        return int(round(base * level / 50. + 5))

    base_pokemon = top_poke[0]
    base_hp = int(base_pokemon.base_hp)
    base_attack = int(base_pokemon.base_attack)
    base_defense = int(base_pokemon.base_defense)
    base_speed = int(base_pokemon.base_speed)
    base_special_attack = int(base_pokemon.base_special_attack)
    base_special_defense = int(base_pokemon.base_special_defense)
    type_1 = str(base_pokemon.type_1)
    type_2 = str(base_pokemon.type_2)

    # calculate the stats according to pokemon's level
    hp = calculate_hp(base_hp, level)
    attack = calculate_stats(base_attack, level)
    defense = calculate_stats(base_defense, level)
    speed = calculate_stats(base_speed, level)
    special_attack = calculate_stats(base_special_attack, level)
    special_defense = calculate_stats(base_special_defense, level)

    added_pokemon = Pokemon(base_stat = base_pokemon,
                            type_1=type_1,
                            type_2=type_2,
                            user = request.user,
                            level = level,
                            hp = hp,
                            attack = attack,
                            defense = defense,
                            speed = speed,
                            special_attack = special_attack,
                            special_defense = special_defense)
    added_pokemon.save()
    team[0].pokemon.add(added_pokemon)

    return HttpResponseRedirect('/pokemon/team/')

def locate_pokemon(request):
    return None

def locate_pokemon_by_route(route):
    return None

def locate_pokemon_by_route(route):
    return None

def locate_pokemon_by_route(route):
    return None

def piechart(request):
    x = ["Fire", "Water", "Flying", "Normal", "Electric"]
    y = [30,30,20,40,20]

    piechart(request, x, y)
    return HttpResponseRedirect('/pokemon/team/')

def piechart(req, x, y):
    extra_serie = {"tooltip": {"y_start": "", "y_end": " cal"}}
    chartdata = {'x': x, 'y1': y, 'extra1': extra_serie}
    charttype = "pieChart"

    data = {
        'charttype': charttype,
        'chartdata': chartdata,
    }
    return render(req, 'piechart.html', data)

def best_poke_against_type(request):
    query_dict = request.POST

    type = query_dict.get('type').lower().strip()

    query = Strength.objects.all().filter(strength__exact=type)

    quer = Base_Stat.objects.all().order_by(Q('base_attack'))


class Location_poke:

    def __init__(self, n, i, r, ra):
        self.name = n
        self.image = i
        self.route = r
        self.rarity = ra

