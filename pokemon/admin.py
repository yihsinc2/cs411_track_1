# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from pokemon.models import Base_Stat, UserProfile, Team

admin.site.register(Base_Stat)
admin.site.register(UserProfile)
admin.site.register(Team)
#admin.site.register(Page)
#admin.site.register(Person)
# Register your models here.
