from django.conf.urls import url
import views
urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^register/$', views.register, name='register'),
    url(r'^login/$', views.user_login, name='login'),
    url(r'^logout/$', views.user_logout, name='logout'),
    url(r'^search/$', views.search_pokemon, name='search_pokemon'),
    url(r'^search/location/$', views.search_location, name='search_location'),
    url(r'^team/$', views.showTeams, name='show_team'),
    url(r'^team/add/$', views.addTeams, name='add_team'),
    url(r'^team/delete/$', views.deleteTeams, name='delete_team'),
    url(r'^team/addPokemon/$', views.addPokemon, name='add_pokemon'),
    url(r'^team/updatePokemon/$', views.updatePokemon, name='update_pokemon'),
    url(r'^team/pickBestNext/$', views.pickBestNext, name='pickBestNext'),
    url(r'^search/location/$', views.search_location, name='search_location'),
    url(r'^search/search_by_stat/$', views.search_by_stat, name='search_by_stat'),
    url(r'^search/bestAgainst/$', views.search_by_stat, name='search_by_stat'),

]

