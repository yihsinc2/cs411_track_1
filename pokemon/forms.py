from django import forms
from django.contrib.auth.models import User
from models import UserProfile, Base_Stat

class UserForm(forms.ModelForm):
    password = forms.CharField(widget=forms.PasswordInput())
    class Meta:
        model = User
        fields = ('username', 'password', 'first_name', 'last_name')

class UserProfileForm(forms.ModelForm):
    class Meta:
        model = UserProfile
        fields= ('gender', 'region', 'picture')

class PokemonSearchForm(forms.Form):

    TYPE_CHOICES = [('none', 'None'),
                    ('normal', 'Normal'),
                    ('fighting', 'Fighting'),
                    ('flying', 'Flying'),
                    ('poison', 'Poison'),
                    ('ground', 'Ground'),
                    ('rock', 'Rock'),
                    ('bug', 'Bug'),
                    ('ghost', 'Ghost'),
                    ('fire', 'Fire'),
                    ('water', 'Water'),
                    ('grass', 'Grass'),
                    ('electric', 'Electric'),
                    ('psychic', 'Psychic'),
                    ('ice', 'Ice'),
                    ('dragon', 'Dragon')]

    pokedex = forms.IntegerField(required=False)
    pokename = forms.CharField(max_length=50, required=False)
    type_1 = forms.ChoiceField(choices = TYPE_CHOICES, required=False)
    type_2 = forms.ChoiceField(choices = TYPE_CHOICES, required=False)

    class Meta:
        fields = ('pokedex', 'pokename', 'type_1', 'type_2')

class PokemonStatForm(forms.Form):
    STAT_CHOICES = ['hp', 'attack', 'defense', 'special_attack', 'special_defense', 'speed']
    stat = forms.ChoiceField(choices= STAT_CHOICES, required=True)

    class Meta:
        fields = ('stat')