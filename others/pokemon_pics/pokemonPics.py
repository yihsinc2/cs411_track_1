import cv2
import os
import pdb
import numpy as np

files = os.listdir('.')
tgt_dir = 'pokemon_pics'
resolution = 150

if not os.path.exists(tgt_dir):
    os.makedirs(tgt_dir)

for file in files:
    if file.endswith('png'):
        img = cv2.imread(file, cv2.IMREAD_UNCHANGED)
        #b,g,r = cv2.split(img)
        #alpha = b;
        #img_RGBA = cv2.merge((b, g, r, alpha))

        resized_img = cv2.resize(img, (resolution, resolution), interpolation=cv2.INTER_CUBIC)
        filename = file.split('-')[-1]
        cv2.imwrite(os.path.join(tgt_dir, filename), resized_img)

