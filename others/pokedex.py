import numpy as np
import csv
import os
import pdb

TYPES = ['', 'normal', 'fighting', 'flying', 'poison', 'ground', 'rock', 'bug', 'ghost', 'steel', 'fire', 'water',
         'grass', 'electric', 'psychic', 'ice', 'dragon', 'dark', 'fairy']
TYPES_DICT = {'normal':1, 'fighting':1, 'flying':1, 'poison':1, 'ground':1, 'rock':1, 'bug':1, 'ghost':1, 'steel':1, 'fire':1, 'water':1,
         'grass':1, 'electric':1, 'psychic':1, 'ice':1, 'dragon':1, 'dark':1, 'fairy':1}
EFFECTIVENESStest = {'normal':TYPES_DICT, 'fighting':TYPES_DICT, 'flying':TYPES_DICT, 'poison':TYPES_DICT,
                 'ground':TYPES_DICT, 'rock':TYPES_DICT, 'bug':TYPES_DICT, 'ghost':TYPES_DICT, 'steel':TYPES_DICT,
                 'fire':TYPES_DICT, 'water':TYPES_DICT, 'grass':TYPES_DICT, 'electric':TYPES_DICT, 'psychic':TYPES_DICT,
                 'ice':TYPES_DICT, 'dragon':TYPES_DICT, 'dark':TYPES_DICT, 'fairy':TYPES_DICT}
EFFECTIVENESS = np.ones(shape=(18,18), dtype=float)

def is_super_effective(move_type, pokemon_type):
    i, j = move_type, pokemon_type

    anormal = (i == 'normal') and (j in set())
    afighting = i == 'fighting' and (j in {'normal', 'rock', 'steel', 'ice', 'dark'})
    aflying = i == 'flying' and (j in {'fighting', 'bug', 'grass'})
    apoison = i == 'poison' and (j in {'grass', 'fairy'})
    aground = i == 'ground' and (j in {'poison', 'rock', 'steel', 'fire', 'electric'})
    arock = i == 'rock' and (j in {'flying', 'bug', 'fire', 'ice'})
    abug = i == 'bug' and (j in {'grass', 'psychic', 'dark'})
    aghost = i == 'ghost' and (j in {'ghost', 'psychic'})
    asteel = i == 'steel' and (j in {'rock', 'fairy', 'ice'})
    afire = i == 'fire' and (j in {'bug', 'steel', 'ice'})
    awater = i == 'water' and (j in {'ground', 'rock', 'fire'})
    agrass = i == 'grass' and (j in {'ground', 'rock', 'water'})
    aelectric = i == 'electric' and (j in {'flying', 'water'})
    apsychic = i == 'psychic' and (j in {'fighting', 'poison'})
    aice = i == 'ice' and (j in {'flying', 'ground', 'grass', 'dragon'})
    adragon = i == 'dragon' and (j in {'dragon'})
    adark = i == 'dark' and (j in {'ghost', 'psychic'})
    afairy = i == 'fairy' and (j in {'fighting', 'dragon', 'dark'})

    return (anormal or afighting or aflying or apoison or aground or arock or abug or aghost or asteel or
            afire or awater or agrass or aelectric or apsychic or aice or adragon or adark or afairy)

def is_not_very_effective(move_type, pokemon_type):
    i, j = move_type, pokemon_type

    dnormal = i == 'normal' and (j in {'rock', 'steel'})
    dfighting = i == 'fighting' and (j in {'flying', 'poison', 'bug', 'psychic', 'fairy'})
    dflying = i == 'flying' and (j in {'rock', 'steel', 'electric'})
    dpoison = i == 'poison' and (j in {'poison', 'ground', 'rock', 'ghost'})
    dground = i == 'ground' and (j in {'bug', 'grass'})
    drock = i == 'rock' and (j in {'fighting', 'ground', 'steel'})
    dbug = i == 'bug' and (j in {'fighting', 'flying', 'poison', 'ghost', 'steel', 'fire', 'fairy'})
    dghost = i == 'ghost' and (j in {'dark'})
    dsteel = i == 'steel' and (j in {'steel', 'fire', 'water', 'electric'})
    dfire = i == 'fire' and (j in {'rock', 'fire', 'water', 'dragon'})
    dwater = i == 'water' and (j in {'water', 'grass', 'dragon'})
    dgrass = i == 'grass' and (j in {'flying', 'poison', 'bug', 'steel', 'fire', 'grass', 'dragon'})
    delectric = i == 'electric' and (j in {'grass', 'electric', 'dragon'})
    dpsychic = i == 'psychic' and (j in {'steel', 'psychic'})
    dice = i == 'ice' and (j in {'steel', 'fire', 'water', 'ice'})
    ddragon = i == 'dragon' and (j in {'steel'})
    ddark = i == 'dark' and (j in {'fight', 'dark', 'fairy'})
    dfairy = i == 'fairy' and (j in {'poison', 'steel', 'fire'})

    return (dnormal or dfighting or dflying or dpoison or dground or drock or dbug or dghost or dsteel or
         dfire or dwater or dgrass or delectric or dpsychic or dice or ddragon or ddark or dfairy)

def is_not_effective(move_type, pokemon_type):
    i, j = move_type, pokemon_type

    znormal = i == 'normal' and (j in {'ghost'})
    zfighting = i == 'fighting' and (j in {'ghost'})
    zpoison = i == 'poison' and (j in {'steel'})
    zground = i == 'ground' and (j in {'flying'})
    zghost = i == 'ghost' and (j in {'normal'})
    zelectric = i == 'electric' and (j in {'ground'})
    zpsychic = i == 'phychic' and (j in {'dark'})
    zdragon = i == 'dragon' and (j in {'fairy'})

    return (znormal or zfighting or zpoison or zground or zghost or zelectric or zpsychic or zdragon)

def type_effectiveness(move_type, pokemon_type):
    if (is_not_effective(move_type, pokemon_type)):
        return 0
    elif (is_not_very_effective(move_type, pokemon_type)):
        return .5
    elif (is_super_effective(move_type, pokemon_type)):
        return 2
    else:
        return 1

def effectiveness(move_type, pokemon):
    if type(pokemon.type_2) == type(None):
        return type_effectiveness(move_type, pokemon.type_1)
    else:
        return type_effectiveness(move_type, pokemon.type_1) * type_effectiveness(move_type, pokemon.type_2)

class Pokemon:
    pokedex_number = 0
    name = ""
    health_points = 0
    attack = 0
    defense = 0
    special_att = 0
    special_def = 0
    speed = 0
    type_1 = ""
    type_2 = ""
    location = ""       #690

    def __init__(self, pokedex_number, name, health_points, attack, defense, special_att, special_def, speed, type_1, type_2, location):
        self.pokedex_number = pokedex_number
        self.name = name
        self.health_points = health_points
        self.attack = attack
        self.defense = defense
        self.special_att = special_att
        self.special_def = special_def
        self.speed = speed
        self.type_1 = type_1
        self.type_2 = type_2
        self.location = location

pokedex = list()
stats = np.zeros(shape=(151+1, 6+1),dtype=int); #hp, att, def, sp-att, sp-def, speed
strings = np.empty(shape=(152, 4), dtype=object)   # name, type_1, type_2, location

#file
# gathers stats
f = open('pokemon_stats.csv')
csv_f = csv.reader(f)
for line in csv_f:

    if line[0] != 'pokemon_id':
        if int(line[0]) == 152:
            break
        stats[int(line[0])][int(line[1])] = int(line[2])
f.close()

# Gathers names
f = open('pokemon.csv')
csv_f = csv.reader(f)
for line in csv_f:

    if line[0] != 'id':
        x = int(line[0])
        s = line[1]
        if x == 152:
            break
        strings[x][0] = s

f.close()

# Gathers Types
f = open('pokemon_types.csv')
csv_f = csv.reader(f)
for line in csv_f:

    if line[0] != 'pokemon_id':
        if int(line[0]) == 152:
            break
        strings[int(line[0])][int(line[2])] = TYPES[int(line[1])]

f.close()

pokedex = np.empty(shape=152, dtype=Pokemon)

for i in range(1, 152):
    pokedex[i] = Pokemon(i, strings[i][0], stats[i][1], stats[i][2], stats[i][3], stats[i][4], stats[i][5], stats[i][6],
                                          strings[i][1], strings[i][2], strings[i][3])


text_file = open('output.txt', 'w')
index = 1
for poke in pokedex:
    if type(poke) != type(None):
        if type(poke.type_2) == type(None):
            poke.type_2 = ""
        STATIC_PATH = os.path.join(os.path.dirname(os.path.dirname(os.path.abspath(__file__))), 'mysite', "static")
        poke.name = poke.name.capitalize()
        # pdb.set_trace()
        if not os.path.exists(os.path.join(STATIC_PATH, "images/%03d%s.png"%(index, poke.name))):
            print("%s doesn't exist!" % (poke.name))
        text_file.write(poke.name + " " + str(poke.pokedex_number) + " " + str(poke.health_points) + " " + str(poke.attack) +
              " " + str(poke.defense) + " " + str(poke.special_att) + " " + str(poke.special_def) +
              " " + str(poke.speed) + " images/%03d%s.png " % (index, poke.name)  + poke.type_1 + " " + poke.type_2 + "\n")
        index += 1
text_file.close()

