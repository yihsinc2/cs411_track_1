import urllib2
from bs4 import BeautifulSoup
import pdb

url_format = 'https://www.serebii.net/pokedex/location/%03d.shtml'


with open('locations.txt', 'w') as f:
    for pokedex in range(1, 152):
        print ('parsing html file @ ' + url_format % pokedex + '...')
        page = urllib2.urlopen(url_format % pokedex)
        soup = BeautifulSoup(page, 'html.parser')
        info_list = soup.find_all('font', attrs= {'size': '4'})
        # according to the format of this website, only those pokemon which cannot be
        # found in the wild we have a second font tag with size=4

        if len(info_list) == 2:
            print (info_list[1].text.strip())
            continue

        tables = soup.find_all('table', attrs= {'class': 'dextable'})
        for table in tables:
            table_name = table.find('a').text.strip()
            if 'Red' in table_name:

                rows = table.find_all('tr')
                for i, row in enumerate(rows):
                    # skip the first two rows which define the format
                    if i <= 1:
                        continue
                    cols = row.find_all('td')
                    if "Route" in cols[0].text:
                        f.write("%s_%s_%s\n" % (pokedex, cols[0].text.strip().split()[-1], cols[2].text.strip()))
